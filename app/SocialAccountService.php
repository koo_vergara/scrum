<?php

namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;
use Modules\SocialAuth\Http\Controllers\SocialAuthController;
use Config, DB;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $controller = new SocialAuthController;
        $account = SocialAccount::whereProvider('google')->whereProviderUserId($providerUser->getId())->first();
        $isValid = $controller->validateEmailDomain($providerUser->getEmail());

        if($isValid){
            if($account){
                $user = User::whereEmail($providerUser->getEmail())->first();
                return $user;
            }
            else {
                $account = new SocialAccount([
                    'provider_user_id'  => $providerUser->getId(),
                    'provider'          => 'google'
                ]);
               
                $user = User::whereEmail($providerUser->getEmail())->first();

                if(!$user){
                    $user = User::create([
                        'email'            => $providerUser->getEmail(),
                        'first_name'       => $providerUser->user['name']['givenName'],
                        'last_name'        => $providerUser->user['name']['familyName']
                    ]);
                }

                $account->user()->associate($user);
                $account->save();

                return $user;
            }
        }
        elseif(!$isValid){
            return Config::get('constants.errorCodes.EMAIL_NOT_ASSOCIATED');
        }
        else {
            return false;
        }
    }
}