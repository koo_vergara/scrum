<?php

return [
	'sprint_status' => [
		'NEW'		=> 0,
		'ONGOING'	=> 1,
		'PAUSED'	=> 2,
		'CANCELED'	=> 3,
		'DONE'		=> 4
	],
	'complexity' => [
		'SMALL'		=> 2,
		'MEDIUM'	=> 5,
		'LARGE'		=> 13,
		'XLARGE'	=> 34
	],
	'task_log_type' => [
		'TASK_STATUS'	=> 0,
		'TASK_ASSIGNEE' => 1,
		'TASK_HOURS'	=> 2
	],
	'task_status' => [
		'BACKLOG'	=> 0,
		'ONGOING'	=> 1,
		'TEST'		=> 2,
		'DONE'		=> 3
	],
	'holidays' => [
		'June 12'
	],
	'validation' => [
    	'email' => [
    		'ALLOWED_EMAIL_DOMAIN' => ['straightarrow.com.ph']
    	]
	],
	'errorCodes' => [
        'EMAIL_NOT_ASSOCIATED' => 'sac-t-1000000'
    ]
];