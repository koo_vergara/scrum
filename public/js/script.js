$(function(){
	$('.modal').modal();
	$('select').material_select();
	$('.datepicker').pickadate({
		selectMonths: true,
		selectYears: 15,
		today: 'Today',
		clear: 'Clear',
		close: 'Ok',
		closeOnSelect: false,
		container: 'body'
	});

	$('#new-project').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('.edit-project').on('click', function(e){
		$.ajax({
			url: $(this).data('url'),
			success: function(data, status, jqxhr){
				$('#edit_project_id').val(data.id);
				$('#edit_project_name').val(data.project_name);
				$('#edit_repository_url').val(data.repository_url);
				$('#edit_project_desc').val(data.description);
				Materialize.updateTextFields();
				$('#edit-project-modal').modal('open');
			}
		});

		e.preventDefault();
	});

	$('#edit-project').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('#new-sprint').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('.edit-sprint').on('click', function(e){
		$.ajax({
			url: $(this).data('url'),
			success: function(data, status, jqxhr){
				$('#edit_sprint_id').val(data.id);
				$('#edit_sprint_number').val(data.sprint_number);
				$('#edit_project_id').val(data.project_id);
				$('#edit_dev_count').val(data.dev_count);
				$('#edit_dev_hours').val(data.dev_hours);
				$('#edit_start_date').val(data.start);
				Materialize.updateTextFields();
				$('#edit-sprint-modal').modal('open');
			}
		});

		e.preventDefault();
	});

	$('#edit-sprint').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('#new-story').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('#new-task').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('.update-task-status').on('click', function(e){
		$.ajax({
			url: $(this).data('url'),
			success: function(data, status, jqxhr){
				var previous_log = '',
					task_status = ['Backlog', 'Ongoing', 'Test', 'Done'];

				$('#update-task-status-modal table tbody').html('');

				$.each(data.logs, function(idx, val){
					if(val.log_type == 0){
						previous_log = val;

				// 		$('#update-task-status-modal table tbody').append('<tr><td>'+task_status[val.current_val]+'</td><td>'+val.created_at+'</td></tr>');
					}
				});

				$('#update-task-status-modal .task').text(data.task.task);
				$('#update-task-status-modal .hours').text(data.task.hours);
				$('#update-task-status .task_id').val(data.task.id);
				$('#update-task-status .previous_val').val(previous_log.current_val);

				$('#update-task-status .current_status').each(function(idx, val){
					if(previous_log.current_val == $(val).val()){
						$(val).prop('checked', true);
					}
				});

				$('#update-task-status-modal').modal('open');
			}
		});

		e.preventDefault();
	});

	$('#update-task-status').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	$('.update-task-hours').on('click', function(e){
		$.ajax({
			url: $(this).data('url'),
			success: function(data, status, jqxhr){
				var previous_log = '';

				$('#update-task-hours-modal table tbody').html('');

				$.each(data.logs, function(idx, val){
					if(val.log_type == 2){
						previous_log = val;

				// 		$('#update-task-hours-modal table tbody').append('<tr><td>'+val.current_val+'</td><td>'+val.created_at+'</td></tr>');
					}
				});

				$('#update-task-hours-modal .task').text(data.task.task);
				$('#update-task-hours-modal .hours').text(data.task.hours);
				$('#update-task-hours .task_id').val(data.task.id);

				if(previous_log.log_type == 2){
					$('#update-task-hours .previous_val').val(previous_log.current_val);
				}
				else {
					$('#update-task-hours .previous_val').val(0);
				}

				$('#update-task-hours-modal').modal('open');

				$('#update-task-hours #hours_spent').on('change', function(){
					if(previous_log.remaining == null){
						$('#update-task-hours .remaining').val(data.task.hours - $(this).val());
					}
					else {
						$('#update-task-hours .remaining').val(previous_log.remaining - $(this).val());
					}
				});
			}
		});

		e.preventDefault();
	});

	$('#update-task-hours').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

	var constraints_list = {};

	$('.add-constraints').on('click', function(){
		var ctr = $('.constraint').length,
			prev_ctr = ctr - 1,
			field = '<input type="text" placeholder="Constraint" class="validate constraint constraint-'+ctr+'" data-ctr="'+ctr+'">';

		$('.constraint-'+prev_ctr).parent().append(field);

		$('.constraint').on('change', function(){
			addConstraints(constraints_list, $(this));
		});
	});

	var addConstraints = function(constraints_list, el){
		var key = el.data('ctr');

		constraints_list[key] = el.val();

		$('.constraints').val(JSON.stringify(constraints_list));
	}

	$('.constraint').on('change', function(){
		addConstraints(constraints_list, $(this));
	});

	$('.task-action-button').on('click', function(){
		$.ajax({
			url: $(this).data('url'),
			success: function(data, status, jqxhr){
				var previous_hour = '',
					previous_status = '',
					task_status = ['Backlog', 'Ongoing', 'Test', 'Done'];
				
				$.each(data.logs, function(idx, val){
					if(val.log_type == 0){
						previous_status = val;
					}

					if(val.log_type == 2){
						previous_hour = val;

				// 		$('#update-task-hours-modal table tbody').append('<tr><td>'+val.current_val+'</td><td>'+val.created_at+'</td></tr>');
					}
				});

				$('#task-actions-modal .task').text(data.task.task);
				$('#task-actions-modal .hours').text(data.task.hours);
				$('#update-task-hours .task_id').val(data.task.id);
				$('#update-task-status .task_id').val(data.task.id);
				$('#assign-task .task_id').val(data.task.id);
				$('#update-task-status .previous_val').val(previous_status.current_val);

				if(previous_hour.log_type == 2){
					$('#update-task-hours .previous_val').val(previous_hour.current_val);
				}
				else {
					$('#update-task-hours .previous_val').val(0);
				}

				$('#update-task-status .current_status').each(function(idx, val){
					if(previous_status.current_val == $(val).val()){
						$(val).prop('checked', true);
					}
				});

				if(data.dev != null){
					$('#assign-task select option').each(function(idx, val){
						if(data.dev.user_id == $(val).val()){
							$(val).prop('selected', true);
							$('#assign-task select').material_select();
						}
					});
				}
				else {
					$('#assign-task select option.select-dev').prop('selected', true);
					$('#assign-task select').material_select();
				}

				$('#task-actions-modal').modal('open');

				$('#update-task-hours #hours_spent').on('change', function(){
					if(previous_hour.remaining == null){
						$('#update-task-hours .remaining').val(data.task.hours - $(this).val());
					}
					else {
						$('#update-task-hours .remaining').val(previous_hour.remaining - $(this).val());
					}
				});
			}
		});
	});

	$('#assign-task').on('submit', function(e){
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data, status){
				location.reload();
			},
			error: function(data, status, error){
				console.log(data);
				console.log(status);
				console.log(error);
			}
		});

		e.preventDefault();
	});

});