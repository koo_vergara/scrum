<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module Project</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ asset('css/materialize.min.css') }}" media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="{{ asset('css/styles.css') }}">
    </head>
    <body>
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper green darken-3">
                    <a href="#!" class="brand-logo"><img src="{{ asset('images/logo-align.png') }}"></a>
                    <ul class="right hide-on-med-and-down">
                        <!-- <li><a href="sass.html">Sass</a></li> -->
                        <li><a href="{{ url('/login/logout') }}">Logout</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        @yield('content')
        <script src="http://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        // <script src="https://use.fontawesome.com/f6d7ebdbd3.js"></script>
        <script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
    </body>
</html>