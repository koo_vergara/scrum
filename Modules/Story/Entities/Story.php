<?php

namespace Modules\Story\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Story extends Model
{
	use SoftDeletes;

	protected $table = 'stories';
    protected $fillable = ['user_story', 'constraints', 'complexity', 'sprint_id', 'project_id'];
    protected $dates = ['deleted_at'];

    public function sprint()
    {
    	return $this->belongsTo('Modules\Sprint\Entities\Sprint');
    }

    public function tasks()
    {
    	return $this->hasMany('Modules\Task\Entities\Task');
    }
}
