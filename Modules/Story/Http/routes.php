<?php

Route::group(['middleware' => 'api', 'prefix' => 'story', 'namespace' => 'Modules\Story\Http\Controllers'], function()
{
    Route::get('/', 'StoryController@index');
    Route::post('/store', 'StoryController@store');
    Route::get('/{id}', 'StoryController@show');
});
