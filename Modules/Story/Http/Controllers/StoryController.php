<?php

namespace Modules\Story\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Story\Http\Requests\StoryRequest;
use Modules\Story\Entities\Story;
use Config;
use Modules\Sprint\Http\Controllers\SprintController;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('story::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('story::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoryRequest $request)
    {
        $story = Story::create([
            'user_story'    => $request->get('user_story'),
            'constraints'   => $request->get('constraints'),
            'complexity'    => $request->get('complexity'),
            'sprint_id'     => $request->get('sprint_id'),
            'project_id'    => $request->get('project_id')
        ]);

        return response()->json($story, 201);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $story = Story::find($id);
        $sprint = $story->sprint()->first();
        $stories = $sprint->stories()->get();
        $tasks = $story->tasks()->get();
        $hours = 0;

        if($sprint){
            $sprint_days = (new SprintController)->getSprintDays($sprint);
        }
        else {
            $sprint_days = 0;
        }

        foreach($stories as $key => $val){
            if($val->id == $id){
                $active_story = $key;
            }
        }

        foreach($tasks as $task){
            $hours += $task->hours;
        }

        return view('story::show', [
            'story' => $story,
            'sprint' => $sprint,
            'stories' => $stories,
            'active_story' => $active_story,
            'tasks' => $tasks,
            'hours' => $hours,
            'sprint_days' => $sprint_days
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('story::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
