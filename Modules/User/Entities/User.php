<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = ['username','password','first_name','last_name','email'];
    protected $hidden = ['password'];
}
