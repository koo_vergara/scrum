<?php

Route::group(['middleware' => 'api', 'prefix' => 'task', 'namespace' => 'Modules\Task\Http\Controllers'], function()
{
    Route::get('/', 'TaskController@index');
    Route::post('/store', 'TaskController@store');
    Route::get('/show/{id}', 'TaskController@show');
    Route::post('/updateStatus', 'TaskController@updateStatus');
    Route::post('/updateHours', 'TaskController@updateHours');
    Route::post('/assign', 'TaskController@assign');
});
