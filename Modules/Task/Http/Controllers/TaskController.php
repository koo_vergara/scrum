<?php

namespace Modules\Task\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Task\Http\Requests\TaskRequest;
use Modules\Task\Entities\Task;
use Modules\Task\Entities\TaskLog;
use Config;
use Modules\Task\Entities\TaskAssign;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('task::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('task::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(TaskRequest $request)
    {
        $task = Task::create([
            'task'      => $request->get('task'),
            'hours'     => $request->get('hours'),
            'story_id'  => $request->get('story_id')
        ]);

        if($task){
            $log = TaskLog::create([
                'task_id'       => $task->id,
                'log_type'      => Config::get('constants.task_log_type.TASK_STATUS'),
                'previous_val'  => Config::get('constants.task_status.BACKLOG'),
                'current_val'   => Config::get('constants.task_status.BACKLOG')
            ]);
        }

        return response()->json($task, 201);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        $logs = $task->logs($id)->get();
        $dev = $task->devs($id)->latest()->first();
        
        return ['task' => $task, 'logs' => $logs, 'dev' => $dev];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('task::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    // public function storeTaskLog(TaskRequest $request)
    // {
    //     $task = Task::create([
    //         'task'      => $request->get('task'),
    //         'hours'     => $request->get('hours'),
    //         'story_id'  => $request->get('story_id')
    //     ]);

    //     return response()->json($task, 201);
    // }

    public function updateStatus(Request $request)
    {
        $log = TaskLog::create([
            'task_id'       => $request->get('task_id'),
            'log_type'      => $request->get('log_type'),
            'previous_val'  => $request->get('previous_val'),
            'current_val'   => $request->get('current_status')
        ]);

        return response()->json($log, 201);
    }

    public function updateHours(Request $request)
    {
        $log = TaskLog::create([
            'task_id'       => $request->get('task_id'),
            'log_type'      => $request->get('log_type'),
            'previous_val'  => $request->get('previous_val'),
            'current_val'   => $request->get('hours_spent'),
            'remaining'     => $request->get('remaining'),
            'day'           => ($request->get('day_count') > 0) ? $request->get('day_count') : ''
        ]);

        return response()->json($log, 201);
    }

    public function assign(Request $request)
    {
        $assign = TaskAssign::create([
            'task_id'       => $request->get('task_id'),
            'user_id'      => $request->get('dev')
        ]);

        return response()->json($assign, 201);
    }
}
