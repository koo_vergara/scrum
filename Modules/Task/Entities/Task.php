<?php

namespace Modules\Task\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
	use SoftDeletes;

	protected $table = 'tasks';
    protected $fillable = ['task', 'hours', 'story_id'];
    protected $dates = ['deleted_at'];

    public function story()
    {
    	return $this->belongsTo('Modules\Story\Entities\Story');
    }

    public function logs($task_id)
    {
    	return $this->hasMany('Modules\Task\Entities\TaskLog');
    }

    public function devs($task_id)
    {
        return $this->hasMany('Modules\Task\Entities\TaskAssign');
    }
}
