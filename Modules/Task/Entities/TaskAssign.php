<?php

namespace Modules\Task\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskAssign extends Model
{
    use SoftDeletes;

	protected $table = 'assignment';
    protected $fillable = ['task_id', 'user_id'];
    protected $dates = ['deleted_at'];

    public function task()
    {
    	return $this->belongsTo('Modules\Task\Entities\Task');
    }

    public function user()
    {
    	return $this->belongsTo('Modules\User\Entities\User');
    }
}
