<?php

namespace Modules\Task\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskLog extends Model
{
	use SoftDeletes;

	protected $table = 'task_logs';
    protected $fillable = ['task_id', 'log_type', 'previous_val', 'current_val', 'remaining', 'day'];
    protected $dates = ['deleted_at'];

    public function task()
    {
    	return $this->belongsTo('Modules\Task\Entities\Task');
    }
}
