<?php

namespace Modules\SocialAuth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\SocialAccountService;
use Socialite, Config;

class SocialAuthController extends Controller
{
    public function login_via_google()
    {
        return Socialite::driver('google')->redirect();
    }

    public function google_response(SocialAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('google')->stateless()->user());
        
        if($user == Config::get('constants.errorCodes.EMAIL_NOT_ASSOCIATED')){
            $res = $this->errorResponse('Google account should be associated to StraightArrow');
            $this->catchResponse($res);

            return redirect('/login');
        }
        elseif($user == false){
            return redirect('/login');
        }
        else {
            auth()->login($user);

            return redirect('/project');
        }
    }

    public function validateEmailDomain($email)
    {
        $domains = Config::get('constants.validation.email.ALLOWED_EMAIL_DOMAIN');

        foreach($domains as $domain){
            if(strlen($email) > strlen($domain)){
                $pos = strpos($email, $domain, strlen($email) - strlen($domain));

                if($pos === false) continue;
                if($pos == 0 || $email[(int) $pos - 1] == "@" || $email[(int) $pos - 1] == ".") return 1;
            }
        }

        return;
    }

    public function errorResponse($msg, $code = '401')
    {
        return $this->checkResponse($msg, 'error', $code);
    }

    public function catchResponse($res)
    {
        Session::flash('message', [
            'type'      => $res->type,
            'message'   => $res->message
        ]);
    }

    public function successResponse($msg, $code = '200')
    {
        return $this->checkResponse($msg, 'success', $code);
    }

    public function checkResponse($msg, $type, $code)
    {
        $res = [];
        $res = (object)array(
            'code'      => $code,
            'type'      => $type,
            'message'   => $msg,
        );

        return $res;
    }
}
