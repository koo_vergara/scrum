<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\SocialAuth\Http\Controllers'], function()
{
    Route::get('/login/google', 'SocialAuthController@login_via_google');
    Route::get('/login/google/response', 'SocialAuthController@google_response');
});
