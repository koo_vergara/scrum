@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col s2 offset-s10 teal z-depth-2 white-text sticky-top">
			<h5>Total Complexity: {{ $p_velocity }}</h5>
			<h5>Total Hours: {{ $total_hours }}</h5>
		</div>
		<div class="col s12">
			<p><a href='{{ url("/project/$project->id") }}'>&laquo; Back to {{ $project->project_name }}</a></p>
		    <h1>{{ $project->project_name }}: Sprint {{ $sprint->sprint_number }} </h1>

		    <div class="row">
		    	<div class="col s12">
			    	@foreach($stories as $story)
			    	<div class="col s12 m3">
			    		<div class="card small sticky-action blue-grey darken-1 hoverable">
			    			<div class="card-content white-text">
			    				<span class="card-title activator">
			    					{{ $story->complexity }} 
			    					<i class="material-icons right">more_vert</i></span>
			    				<p>{{ $story->user_story }}</p>

			    				@if($story->constraints)
			    				@php
			    					$constraints = json_decode($story->constraints);
			    				@endphp
			    				<ul class="browser-default">
			    					@foreach($constraints as $constraint)
			    					<li>{{ $constraint }}</li>
			    					@endforeach
			    				</ul>
			    				@endif
			    			</div>
			    			<div class="card-action">
			    				<a href='{{ url("/story/$story->id") }}'><i class="material-icons">visibility</i></a>
			    			</div>
			    			<div class="card-reveal">
			    				<span class="card-title grey-text text-darken-4">Tasks <i class="material-icons right">close</i></span>
			    				@if($story->tasks()->count())
			    				<ol>
			    					@foreach($story->tasks()->get() as $tasks)
			    					<li>{{ $tasks->task }}</li>
			    					@endforeach
			    				</ol>
			    				@else
			    				<p>There are no tasks in this story.</p>
			    				@endif
			    			</div>
			    		</div>
			    	</div>
			    	@endforeach
			    </div>
		    </div>

		    <div class="fixed-action-btn horizontal click-to-toggle">
		    	<a class="btn-floating btn-large red"><i class="material-icons">more_vert</i></a>
				<ul>
					<li><button data-target="new-story-modal" class="btn-floating green waves-effect waves-light"><i class="material-icons">add</i></button></li>
					<li><button data-target="burndown-chart-modal" class="btn-floating blue waves-effect waves-light"><i class="material-icons">multiline_chart</i></button></li>
				</ul>
		    </div>

		    <div id="new-story-modal" class="modal">
				<div class="modal-content">
					<h4>New User Story</h4>
					<p>Project: {{ $project->project_name }}</p>
					<form method="post" action="{{ url('/story/store') }}" class="col s12" id="new-story">
		                <div class="row">
		                    <div class="input-field col s12">
		                        <textarea id="user_story" name="user_story" placeholder="As a <role>, I <action>, so that <achievement>." class="materialize-textarea"></textarea>
		                        <label for="user_story">User Story</label>
		                    </div>
		                    <div class="col s12">
		                    	<label>Constraints <a class="btn-floating btn-small add-constraints waves-effect waves-light red"><i class="material-icons md-18">add</i></a></label>
		                    	<input type="hidden" name="constraints" class="constraints" value="">
		                    	<input type="text" placeholder="Constraint" class="validate constraint constraint-0" data-ctr="0">
		                    </div>
		                    <div class="input-field col s8">
		                        <div class="row">
		                        	<div class="col s3">
		                        		<input class="with-gap" name="complexity" type="radio" id="small" value="2">
      									<label for="small">Small</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="medium" value="5">
      									<label for="medium">Medium</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="large" value="13">
      									<label for="large">Large</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="xlarge" value="34">
      									<label for="xlarge">XLarge</label>
      								</div>
      							</div>
		                    </div>
		                    <div class="input-field col s12">
		                    	<input type="hidden" name="sprint_id" value="{{ $sprint->id }}">
		                    	<button class="btn waves-effect waves-light" type="submit" name="action">Add</button>
		                	</div>
		                </div>
		            </form>
				</div>
			</div>

			<div id="burndown-chart-modal" class="modal">
				<div class="modal-content">
					<h4>Burndown Chart</h4>
					<canvas id="burndown"></canvas>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
	<!--
	<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('js/ckeditor/config.js') }}"></script>
	<script src="{{ asset('js/ckeditor/lang/en.js') }}"></script>
	<script src="{{ asset('js/ckeditor/styles.js') }}"></script>
	-->

	<script>
	<?php
	$labels = [];
	$projected = [$total_hours];
	$hours_left = $total_hours;

	for($i = 0; $i <= $sprint_days; $i++){
		$labels[] = "Day ".$i;
		$hours_left = $hours_left - $day_hours;

		if($hours_left < 0){
			$projected[] = 0;	
		}
		else {
			$projected[] = $hours_left;
		}
	}
	?>
	var ctx = document.getElementById("burndown");
	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: <?php echo json_encode($labels); ?>,
	        datasets: [{
				label: "Actual",
				fillColor: 'rgba(18,78,217,0.5)',
				borderColor: 'rgba(18,78,217,1)',
				pointColor: 'rgba(18,78,217,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(18,78,217,1)',
				data: <?php echo json_encode($actual); ?>
	        }, {
				label: "Projected",
	            fillColor: 'rgba(170,57,57,0.2)',
				borderColor: 'rgba(170,57,57,1)',
				pointColor: 'rgba(170,57,57,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(170,57,57,1)',
				data: <?php echo json_encode($projected); ?>
	        }, {
	        	label: "Max",
	            fillColor: 'rgba(220,220,220,0.2)',
				borderColor: 'rgba(220,220,220,1)',
				pointColor: 'rgba(220,220,220,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: <?php echo json_encode($ideal); ?>
	        }]
	    },
	    options: {
	    	title: {
	    		text: "Burndown Chart",
	    		display: true
	    	},
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        },
	        elements: {
	        	line: {
	        		tension: 0
	        	}
	        }
	    }
	});
	</script>
@stop
