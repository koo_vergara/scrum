@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col s2 offset-s10 purple darken-3 z-depth-2 white-text sticky-top">
			<h5>Total Complexity: {{ $p_velocity }}</h5>
			<h5>Total Hours: {{ $total_hours }}</h5>
			<h6>Savings: {{ $unused_hours }}</h6>
		</div>
		<div class="col s12">
			<p><a href='{{ url("/project/$project->id") }}'>&laquo; Back to {{ $project->project_name }}</a></p>
		    <h1 class="white-text">{{ $project->project_name }}: Sprint {{ $sprint->sprint_number }} </h1>

		    <div class="row scrumboard">
		    	<div class="col s12">
			    	@foreach($stories as $story)
			    	<hr>
			    	<div class="row">
				    	<div class="col m2">
				    		<div class="card sticky-action blue-grey darken-1 hoverable">
				    			<div class="card-content white-text">
				    				<span class="card-title activator">
				    					{{ $story->complexity }} 
				    					<i class="material-icons right">more_vert</i></span>
				    				<p>{{ $story->user_story }}</p>

				    				@if($story->constraints)
				    				@php
				    					$constraints = json_decode($story->constraints);
				    				@endphp
				    				<ul class="browser-default">
				    					@foreach($constraints as $constraint)
				    					<li>{{ $constraint }}</li>
				    					@endforeach
				    				</ul>
				    				@endif
				    			</div>
				    			<div class="card-action">
				    				<a href='{{ url("/story/$story->id") }}'><i class="material-icons">visibility</i></a>
				    			</div>
				    			<div class="card-reveal">
				    				<span class="card-title grey-text text-darken-4">Tasks <i class="material-icons right">close</i></span>
				    				@if($story->tasks()->count())
				    				<ol>
				    					@foreach($story->tasks()->get() as $tasks)
				    					<li>{{ $tasks->task }}</li>
				    					@endforeach
				    				</ol>
				    				@else
				    				<p>There are no tasks in this story.</p>
				    				@endif
				    			</div>
				    		</div>
				    	</div>
				    	<div class="col m10">
				    		<div class="col m3">
				    			<p class="white-text"><strong>BACKLOG</strong></p>
				    			<ul class="collection">
		    					@foreach($story->tasks()->get() as $task)
		    						<?php
				    				$log = '';
				    				$hours_spent = 0;
				    				$dev = $task->devs($task->id)->latest()->first();
        							$name = (is_null($dev)) ? '' : $dev->user()->first()->first_name;
				    				
				    				foreach($task->logs($task->id)->get() as $logs){
				    					if($logs->log_type == 0){
				    						$log = $logs;
				    					}

										if($logs->log_type == 2){
											$hours_spent += $logs->current_val;
										}
				    				}
					    			?>
					    			@if($log->current_val == 0)
					    			<li class="collection-item row">
					    				<div class="col s11">
		    								@if(!is_null($dev))
		    								<div class="chip">{{ $name }}</div>
		    								<br>
		    								@endif
		    								<strong>{{ $hours_spent }}/{{ $task->hours }}</strong> - {{ $task->task }}
		    							</div>
		    							<div class="col s1">
		    								<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="secondary-content task-action-button"><i class="material-icons">more_vert</i></a>
		    							</div>
					    			</li>
		    						@endif
		    					@endforeach
		    					</ul>
				    		</div>
				    		<div class="col m3">
				    			<p class="white-text"><strong>ONGOING</strong></p>
				    			<ul class="collection">
				    			@foreach($story->tasks()->get() as $task)
		    						<?php
				    				$log = '';
				    				$hours_spent = 0;
				    				$dev = $task->devs($task->id)->latest()->first();
        							$name = (is_null($dev)) ? '' : $dev->user()->first()->first_name;
				    				
				    				foreach($task->logs($task->id)->get() as $logs){
				    					if($logs->log_type == 0){
				    						$log = $logs;
				    					}

										if($logs->log_type == 2){
											$hours_spent += $logs->current_val;
										}
				    				}
					    			?>
					    			@if($log->current_val == 1)
		    						<li class="collection-item row">
					    				<div class="col s11">
		    								@if(!is_null($dev))
		    								<div class="chip">{{ $name }}</div>
		    								<br>
		    								@endif
		    								<strong>{{ $hours_spent }}/{{ $task->hours }}</strong> - {{ $task->task }}
		    							</div>
		    							<div class="col s1">
		    								<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="secondary-content task-action-button"><i class="material-icons">more_vert</i></a>
		    							</div>
					    			</li>
		    						@endif
		    					@endforeach
		    					</ul>
				    		</div>
				    		<div class="col m3">
				    			<p class="white-text"><strong>TEST</strong></p>
				    			<ul class="collection">
				    			@foreach($story->tasks()->get() as $task)
		    						<?php
				    				$log = '';
				    				$hours_spent = 0;
				    				$dev = $task->devs($task->id)->latest()->first();
        							$name = (is_null($dev)) ? '' : $dev->user()->first()->first_name;
				    				
				    				foreach($task->logs($task->id)->get() as $logs){
				    					if($logs->log_type == 0){
				    						$log = $logs;
				    					}

										if($logs->log_type == 2){
											$hours_spent += $logs->current_val;
										}
				    				}
					    			?>
					    			@if($log->current_val == 2)
		    						<li class="collection-item row">
					    				<div class="col s11">
		    								@if(!is_null($dev))
		    								<div class="chip">{{ $name }}</div>
		    								<br>
		    								@endif
		    								<strong>{{ $hours_spent }}/{{ $task->hours }}</strong> - {{ $task->task }}
		    							</div>
		    							<div class="col s1">
		    								<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="secondary-content task-action-button"><i class="material-icons">more_vert</i></a>
		    							</div>
					    			</li>
		    						@endif
		    					@endforeach
		    					</ul>
				    		</div>
				    		<div class="col m3">
				    			<p class="white-text"><strong>DONE</strong></p>
				    			<ul class="collection">
				    			@foreach($story->tasks()->get() as $task)
		    						<?php
				    				$log = '';
				    				$hours_spent = 0;
				    				$dev = $task->devs($task->id)->latest()->first();
				    				$name = (is_null($dev)) ? '' : $dev->user()->first()->first_name;
				    				
				    				foreach($task->logs($task->id)->get() as $logs){
				    					if($logs->log_type == 0){
				    						$log = $logs;
				    					}

										if($logs->log_type == 2){
											$hours_spent += $logs->current_val;
										}
				    				}
					    			?>
					    			@if($log->current_val == 3)
		    						<li class="collection-item row">
					    				<div class="col s11">
					    					@if(!is_null($dev))
		    								<div class="chip">{{ $name }}</div>
		    								<br>
		    								@endif
		    								<strong>{{ $hours_spent }}/{{ $task->hours }}</strong> - {{ $task->task }}
		    							</div>
		    							<div class="col s1">
		    								<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="secondary-content task-action-button"><i class="material-icons">more_vert</i></a>
		    							</div>
					    			</li>
		    						@endif
		    					@endforeach
		    					</ul>
				    		</div>
				    	</div>
				    </div>
			    	@endforeach
			    </div>
		    </div>

		    <div class="fixed-action-btn horizontal click-to-toggle">
		    	<a class="btn-floating btn-large red"><i class="material-icons">more_vert</i></a>
				<ul>
					<li><button data-target="new-story-modal" class="btn-floating green waves-effect waves-light"><i class="material-icons">add</i></button></li>
					<li><button data-target="burndown-chart-modal" class="btn-floating blue waves-effect waves-light"><i class="material-icons">multiline_chart</i></button></li>
				</ul>
		    </div>

		    <div id="task-actions-modal" class="modal">
				<div class="modal-content">
					<div class="row">
						<div class="col s6">
							<strong>Task:</strong> <span class="task"></span>
						</div>
						<div class="col s6">
							<strong>Hours:</strong> <span class="hours"></span>
						</div>
					</div>
					<div class="row">
						<div class="col s4">
							<form method="post" action="{{ url('/task/updateStatus') }}" class="col s12" id="update-task-status">
			                	<div class="row">
			                		<div class="col s6">
										<input class="with-gap current_status" name="current_status" type="radio" id="status-backlog" value="0" />
										<label for="status-backlog">Backlog</label>
									</div>
									<div class="col s6">
										<input class="with-gap current_status" name="current_status" type="radio" id="status-ongoing" value="1" />
										<label for="status-ongoing">Ongoing</label>
									</div>
								</div>
								<div class="row">
			                		<div class="col s6">
										<input class="with-gap current_status" name="current_status" type="radio" id="status-test" value="2" />
										<label for="status-test">Test</label>
									</div>
									<div class="col s6">
										<input class="with-gap current_status" name="current_status" type="radio" id="status-done" value="3" />
										<label for="status-done">Done</label>
									</div>
								</div>
			                    <div class="input-field col s12">
			                    	<input type="hidden" class="task_id" name="task_id" value="">
			                    	<input type="hidden" class="log_type" name="log_type" value="0">
			                    	<input type="hidden" class="previous_val" name="previous_val" value="">
			                    	<button class="btn waves-effect waves-light" type="submit" name="action">Update Status</button>
			                	</div>
							</form>
						</div>
						<div class="col s4">
							<form method="post" action="{{ url('/task/updateHours') }}" class="col s12" id="update-task-hours">
								<div class="input-field col s6">
			                        <input id="hours_spent" name="hours_spent" type="number" step="0.5" class="validate" required="" aria-required="" value="0" min="0">
									<label for="hours_spent">Hours Spent</label>
								</div>
								<div class="input-field col s6">
			                        <input id="day_count" name="day_count" type="number" class="validate" required="" aria-required="" value="0" min="0">
									<label for="day_count">Day</label>
								</div>
			                    <div class="input-field col s12">
			                    	<input type="hidden" class="task_id" name="task_id" value="">
			                    	<input type="hidden" class="log_type" name="log_type" value="2">
			                    	<input type="hidden" class="previous_val" name="previous_val" value="">
			                    	<input type="hidden" class="remaining" name="remaining" value="">
			                    	<button class="btn waves-effect waves-light" type="submit" name="action">Update Hours</button>
			                	</div>
							</form>
				        </div>
				        <div class="col s4">
				        	<form method="post" action="{{ url('/task/assign') }}" class="col s12" id="assign-task">
				        		<div class="input-field col s12">
				        			<select name="dev">
				        				<option class="select-dev">Select a Developer</option>
				        				@foreach($users as $user)
				        				<option value="{{ $user->id }}">{{ $user->first_name }}</option>
				        				@endforeach
				        			</select>
				        			<label>Assign To</label>
				        		</div>
				        		<div class="input-field col s12">
				        			<input type="hidden" class="task_id" name="task_id" value="">
				        			<button class="btn waves-effect waves-light" type="submit" name="action">Assign</button>
				        		</div>
				        	</form>
				        </div>
					</div>
				</div>
			</div>

		    <div id="new-story-modal" class="modal">
				<div class="modal-content">
					<h4>New User Story</h4>
					<p>Project: {{ $project->project_name }}</p>
					<form method="post" action="{{ url('/story/store') }}" class="col s12" id="new-story">
		                <div class="row">
		                    <div class="input-field col s12">
		                        <textarea id="user_story" name="user_story" placeholder="As a <role>, I <action>, so that <achievement>." class="materialize-textarea"></textarea>
		                        <label for="user_story">User Story</label>
		                    </div>
		                    <div class="col s12">
		                    	<label>Constraints <a class="btn-floating btn-small add-constraints waves-effect waves-light red"><i class="material-icons md-18">add</i></a></label>
		                    	<input type="hidden" name="constraints" class="constraints" value="">
		                    	<input type="text" placeholder="Constraint" class="validate constraint constraint-0" data-ctr="0">
		                    </div>
		                    <div class="input-field col s8">
		                        <div class="row">
		                        	<div class="col s3">
		                        		<input class="with-gap" name="complexity" type="radio" id="small" value="2">
      									<label for="small">Small</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="medium" value="5">
      									<label for="medium">Medium</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="large" value="13">
      									<label for="large">Large</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="xlarge" value="34">
      									<label for="xlarge">XLarge</label>
      								</div>
      							</div>
		                    </div>
		                    <div class="input-field col s12">
		                    	<input type="hidden" name="sprint_id" value="{{ $sprint->id }}">
		                    	<button class="btn waves-effect waves-light" type="submit" name="action">Add</button>
		                	</div>
		                </div>
		            </form>
				</div>
			</div>

			<div id="burndown-chart-modal" class="modal">
				<div class="modal-content">
					<h4>Burndown Chart</h4>
					<canvas id="burndown"></canvas>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
	<!--
	<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset('js/ckeditor/config.js') }}"></script>
	<script src="{{ asset('js/ckeditor/lang/en.js') }}"></script>
	<script src="{{ asset('js/ckeditor/styles.js') }}"></script>
	-->

	<script>
	<?php
	$labels = [];
	$projected = [$total_hours];
	$hours_left = $total_hours;

	for($i = 0; $i <= $sprint_days; $i++){
		$labels[] = "Day ".$i;
		$hours_left = $hours_left - $day_hours;

		if($hours_left < 0){
			$projected[] = 0;	
		}
		else {
			$projected[] = $hours_left;
		}
	}
	?>
	var ctx = document.getElementById("burndown");
	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: <?php echo json_encode($labels); ?>,
	        datasets: [{
				label: "Actual",
				fillColor: 'rgba(18,78,217,0.5)',
				borderColor: 'rgba(18,78,217,1)',
				pointColor: 'rgba(18,78,217,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(18,78,217,1)',
				data: <?php echo json_encode($actual); ?>
	        }, {
				label: "Projected",
	            fillColor: 'rgba(170,57,57,0.2)',
				borderColor: 'rgba(170,57,57,1)',
				pointColor: 'rgba(170,57,57,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(170,57,57,1)',
				data: <?php echo json_encode($projected); ?>
	        }, {
	        	label: "Max",
	            fillColor: 'rgba(220,220,220,0.2)',
				borderColor: 'rgba(220,220,220,1)',
				pointColor: 'rgba(220,220,220,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: <?php echo json_encode($ideal); ?>
	        }]
	    },
	    options: {
	    	title: {
	    		text: "Burndown Chart",
	    		display: true
	    	},
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true
	                }
	            }]
	        },
	        elements: {
	        	line: {
	        		tension: 0
	        	}
	        }
	    }
	});
	</script>
@stop
