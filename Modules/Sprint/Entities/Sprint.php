<?php

namespace Modules\Sprint\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sprint extends Model
{
	use SoftDeletes;

	protected $table = 'sprints';
    protected $fillable = ['sprint_number', 'status', 'project_id', 'dev_count', 'dev_hours', 'start_date'];
    protected $dates = ['deleted_at'];

    public function project()
    {
    	return $this->belongsTo('Modules\Project\Entities\Project');
    }

    public function stories()
    {
    	return $this->hasMany('Modules\Story\Entities\Story');
    }
}
