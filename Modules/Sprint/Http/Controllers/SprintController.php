<?php

namespace Modules\Sprint\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Sprint\Http\Requests\SprintRequest;
use Modules\Sprint\Entities\Sprint;
use Config, Exception, Carbon\Carbon;
use Modules\Story\Entities\Story;
use Modules\User\Entities\User;

class SprintController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sprint::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sprint::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(SprintRequest $request)
    {
        $status = Config::get('constants.sprint_status.NEW');
        $sprint = Sprint::create([
            'sprint_number' => $request->get('sprint_number'),
            'status'        => $status,
            'project_id'    => $request->get('project_id'),
            'dev_count'     => $request->get('dev_count'),
            'dev_hours'     => $request->get('dev_hours'),
            'start_date'    => (is_null($request->get('start_date'))) ? '' : date('Y-m-d', strtotime($request->get('start_date')))
        ]);

        return response()->json($sprint, 201);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, Request $request)
    {
        $view = $request->get('view');
        $sprint = Sprint::find($id);
        $project = $sprint->project()->first();
        $stories = $sprint->stories()->get();
        $p_velocity = 0;
        $burndown = $this->getBurndown($sprint);
        $users = User::all();

        foreach($stories as $story){
            $p_velocity += $story->complexity;
        }

        // if(is_null($view)){
        //     return view('sprint::show', [
        //         'project'       => $project,
        //         'sprint'        => $sprint,
        //         'stories'       => $stories,
        //         'p_velocity'    => $p_velocity,
        //         'sprint_days'   => $burndown['sprint_days'],
        //         'total_hours'   => $burndown['total_hours'],
        //         'day_hours'     => $burndown['day_hours'],
        //         'ideal'         => $burndown['ideal'],
        //         'actual'        => $burndown['burndown']
        //     ]);
        // }
        // else {
            return view('sprint::scrumboard', [
                'project'       => $project,
                'sprint'        => $sprint,
                'stories'       => $stories,
                'p_velocity'    => $p_velocity,
                'sprint_days'   => $burndown['sprint_days'],
                'total_hours'   => $burndown['total_hours'],
                'day_hours'     => $burndown['day_hours'],
                'ideal'         => $burndown['ideal'],
                'actual'        => $burndown['burndown'],
                'unused_hours'  => $burndown['unused_hours'],
                'users'         => $users
            ]);
        // }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $sprint = Sprint::find($id);
        $sprint->start = Carbon::parse($sprint->start_date)->format('j F, Y');

        return $sprint;
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            $sprint = Sprint::where('id', $request->get('sprint_id'))->update([
                'sprint_number' => $request->get('sprint_number'),
                'dev_count'     => $request->get('dev_count'),
                'dev_hours'     => $request->get('dev_hours'),
                'start_date'    => date('Y-m-d', strtotime($request->get('start_date')))
            ]);

            if($sprint){
                $status = 'success';
                $data = $sprint;
            }
            else {
                $status = 'error';
                $data = ['message' => 'Error updating sprint'];
            }
        }
        catch(Exception $e){
            $status = 'error';
            $data = ['code' => $e->getErrorCode(), 'message' => $e->getMessage()];
        }

        return response()->json(['status' => $status, 'data' => $data], 201);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getSprintDays($sprint)
    {
        $stories = $sprint->stories()->get();
        $dev_count = $sprint->dev_count;
        $dev_hours = $sprint->dev_hours;
        $day_hours = $dev_count * $dev_hours;
        $total_hours = 0;

        foreach($stories as $story){
            $tasks = $story->tasks()->get();
            $task_hours = 0;

            foreach($tasks as $task){
                $task_hours += $task->hours;
            }

            $total_hours += $task_hours;
        }

        return ceil($total_hours / $day_hours);
    }

    public function getBurndown($sprint)
    {
        $stories = $sprint->stories()->get();
        $dev_count = $sprint->dev_count;
        $dev_hours = $sprint->dev_hours;
        $day_hours = $dev_count * $dev_hours;
        $total_hours = 0;
        $start_date = strtotime($sprint->start_date);
        $burndown = [];
        
        foreach($stories as $story){
            $tasks = $story->tasks()->get();
            $task_hours = 0;

            foreach($tasks as $task){
                $task_hours += $task->hours;
            }

            $total_hours += $task_hours;
        }

        $sprint_days = ceil($total_hours / $day_hours);
        $holiday_dates = Config::get('constants.holidays');
        $holidays = [];
        $remaining_hours = $total_hours;
        $savings = $this->getSavings($stories);

        foreach($holiday_dates as $h){
            $holidays[] = strtotime(date('Y-m-d', strtotime($h)));
        }

        for($sd = 0; $sd <= $sprint_days; $sd++){
            $date = strtotime("$sprint->start_date +$sd weekday");
            $daily_spent = 0;

            if(!in_array($date, $holidays)){
                foreach($stories as $story){
                    $tasks = $story->tasks()->get();
                    $unused_hours = 0;

                    foreach($tasks as $task){
                        $logs = $task->logs($task->id)->get();

                        foreach($logs as $log){
                            if($log->log_type == Config::get('constants.task_log_type.TASK_HOURS')){
                                if($log->day == $sd){
                                    if($log->remaining >= 0){
                                        $daily_spent += $log->current_val;
                                    }
                                    else {
                                        $daily_spent += $log->current_val - abs($log->remaining);

                                        if(!in_array($log->task_id, $savings['task_ids'])){
                                            $savings['hours'] += $log->remaining;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $remaining_hours -= $daily_spent;
                $burndown[] = $remaining_hours;
            }

        }

        $ideal = [];

        for($d = $sprint_days; $d >= 0; $d--){
            $ideal[] = $d * $day_hours;
        }

        return [
            'sprint_days' => $sprint_days,
            'total_hours' => $total_hours,
            'day_hours' => $day_hours,
            'ideal' => $ideal,
            'burndown' => $burndown,
            'unused_hours' => $savings['hours']
        ];
    }

    public function backlog()
    {
        return view('sprint::backlog');
    }

    public function getSavings($stories)
    {
        $savings = [
            'task_ids' => [],
            'hours' => 0
        ];

        foreach($stories as $story){
            $tasks = $story->tasks()->get();

            foreach($tasks as $task){
                $logs = $task->logs($task->id)->get();
                $last_remaining = $task->hours;

                foreach($logs as $log){
                    if($log->log_type == Config::get('constants.task_log_type.TASK_STATUS') && $log->current_val >= 2){
                        if(!in_array($log->task_id, $savings['task_ids'])){
                            $savings['task_ids'][] = $log->task_id;
                        }
                    }
                }

                foreach($logs as $log){
                    if(in_array($log->task_id, $savings['task_ids'])){
                        if($log->log_type == Config::get('constants.task_log_type.TASK_HOURS')){
                            $last_remaining = $log->remaining;
                        }
                    }
                    else {
                        $last_remaining = 0;
                    }
                }

                $savings['hours'] += $last_remaining;
            }
        }

        return $savings;
    }
}
