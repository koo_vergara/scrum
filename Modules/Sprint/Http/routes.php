<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'sprint', 'namespace' => 'Modules\Sprint\Http\Controllers'], function()
{
    Route::get('/', 'SprintController@index');
    Route::post('/store', 'SprintController@store');
    Route::get('/{id}', 'SprintController@show');
    Route::get('/edit/{id}', 'SprintController@edit');
    Route::post('/edit', 'SprintController@update');
});
