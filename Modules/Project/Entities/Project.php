<?php

namespace Modules\Project\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
	use SoftDeletes;

	protected $table = 'projects';
    protected $fillable = ['project_name', 'description', 'repository_url', 'created_by'];
    protected $dates = ['deleted_at'];

    public function sprints()
    {
    	return $this->hasMany('Modules\Sprint\Entities\Sprint');
    }
}
