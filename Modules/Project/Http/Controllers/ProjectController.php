<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Project\Http\Requests\ProjectRequest;
use Modules\Project\Entities\Project;
use Modules\Story\Entities\Story;
use Modules\User\Entities\User;
use Exception, Auth, Carbon\Carbon;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $projects = Project::all()->map(function($project, $key){
            $sprints = $project->sprints()->get();
            
            if($sprints->count()){
                $project->forecast = $this->getForecast($sprints);
            }
            else {
                $project->forecast = [
                    'project' => ['result' => 'NO DATA', 'color' => ''],
                    'budget' => ['result' => 'NO DATA', 'color' => '']
                ];
            }

            $project->creator = User::find($project->created_by)->first()->first_name;

            return $project;
        });

        return view('project::index', ['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('project::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ProjectRequest $request)
    {
        $project = Project::create([
            'project_name'      => $request->get('project_name'),
            'description'       => $request->get('project_desc'),
            'repository_url'    => $request->get('repository_url'),
            'created_by'        => Auth::id()
        ]);
        
        return response()->json($project, 201);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        $sprints = $project->sprints()->get()->map(function($sprint, $key){
            if($sprint->start_date){
                $sprint->start = Carbon::parse($sprint->start_date)->format('j M Y');
            }
            else {
                $sprint->start = '';
            }

            return $sprint;
        });

        if($sprints->count()){
            $project->forecast = $this->getForecast($sprints);
        }
        else {
            $project->forecast = [
                'project' => ['result' => 'NO DATA', 'color' => ''],
                'budget' => ['result' => 'NO DATA', 'color' => '']
            ];
        }

        $project_hours = $this->getProjectHours($sprints);

        return view('project::show', [
            'project' => $project,
            'sprints' => $sprints,
            'project_hours' => $project_hours
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        return Project::find($id);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            $project = Project::where('id', $request->get('project_id'))->update([
                'project_name'      => $request->get('project_name'),
                'description'       => $request->get('project_desc'),
                'repository_url'    => $request->get('repository_url')
            ]);

            if($project){
                $status = 'success';
                $data = $project;
            }
            else {
                $status = 'error';
                $data = ['message' => 'Error updating project'];
            }
        }
        catch(Exception $e){
            $status = 'error';
            $data = ['code' => $e->getErrorCode(), 'message' => $e->getMessage()];
        }

        return response()->json(['status' => $status, 'data' => $data], 201);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getProjectHours($sprints)
    {
        $project_hours = 0;

        foreach($sprints as $sprint){
            $stories = $sprint->stories()->get();

            foreach($stories as $story){
                $tasks = $story->tasks()->get();

                foreach($tasks as $task){
                    $project_hours += $task->hours;
                }
            }
        }

        return $project_hours;
    }

    public function getForecast($sprints)
    {
        $forecast = [
            'project' => ['result' => 'NO DATA', 'color' => ''],
            'budget' => ['result' => 'NO DATA', 'color' => '']
        ];
        $project_hours = $this->getProjectHours($sprints);
        $total_sprints = $sprints->count();

        if($sprints->count()){
            $planned_value = (1/$total_sprints) * $project_hours;
        }

        $actual_percent_completed = [];
        $earned_value = [];
        $cost_performance_index = [];
        $total_complexity = 0;
        $project_forecast = '';
        $project_forecast_color = '';
        $budget_forecast = '';
        $budget_forecast_color = '';

        foreach($sprints as $sprint){
            $stories = $sprint->stories()->get();
            $complexity = 0;

            foreach($stories as $story){
                $complexity += $story->complexity;
            }

            $total_complexity += $complexity;
        }

        foreach($sprints as $sprint){
            $stories = $sprint->stories()->get();
            $velocity = 0;
            $actual = 0;
            $hours = 0;

            foreach($stories as $story){
                $tasks = $story->tasks()->get();
                $completed_tasks = 0;

                foreach($tasks as $task){
                    $hours += $task->hours;
                    $task_logs = $task->logs($task->id)->where('log_type', 0)->orderBy('id', 'desc')->get();

                    foreach($task_logs as $task_log){
                        if($task_log->current_val >= 3){
                            $completed_tasks += 1;
                        }
                    }

                    $hours_logs = $task->logs($task->id)->where('log_type', 2)->get();

                    foreach($hours_logs as $hours_log){
                        $actual += $hours_log->current_val;
                    }
                }

                if($tasks->count() == $completed_tasks){
                    $velocity += $story->complexity;
                }
            }

            if($actual){
                $actual_percent_completed[$sprint->sprint_number] = $velocity / $total_complexity;
                $earned_value[$sprint->sprint_number] = $actual_percent_completed[$sprint->sprint_number] * $project_hours;

                if($earned_value[$sprint->sprint_number] == $planned_value){
                    $forecast['project']['result'] = 'ON TIME';
                    $forecast['project']['color'] = 'green';
                }
                elseif($earned_value[$sprint->sprint_number] > $planned_value){
                    $forecast['project']['result'] = 'AHEAD';
                    $forecast['project']['color'] = 'green';
                }
                elseif($earned_value[$sprint->sprint_number] < $planned_value){
                    $forecast['project']['result'] = 'DELAYED';
                    $forecast['project']['color'] = 'red';
                }
                else {
                    $forecast['project']['result'] = 'NO DATA';
                    $forecast['project']['color'] = '';
                }

                $cost_performance_index[$sprint->sprint_number] = round(($hours / $actual), 2);

                if($cost_performance_index[$sprint->sprint_number] == 1){
                    $forecast['budget']['result'] = 'ON BUDGET';
                    $forecast['budget']['color'] = 'green';
                }
                elseif($cost_performance_index[$sprint->sprint_number] > 1){
                    $forecast['budget']['result'] = 'COST EFFICIENT';
                    $forecast['budget']['color'] = 'green';
                }
                elseif($cost_performance_index[$sprint->sprint_number] < 1){
                    $forecast['budget']['result'] = 'OVERBUDGET';
                    $forecast['budget']['color'] = 'red';
                }
                else {
                    $forecast['budget']['result'] = 'NO DATA';
                    $forecast['budget']['color'] = '';
                }
            }
        }

        return $forecast;
    }

    public function getPlannedValue()
    {

    }

    public function addBacklog($id)
    {
        $project = Project::find($id);
        $stories = Story::where('sprint_id', null)->where('project_id', $id)->get();

        return view('project::backlog_story', [
            'project' => $project,
            'stories' => $stories
        ]);
    }

    public function showBacklogStory($project_id, $story_id)
    {
        $project = Project::find($project_id);
        $story = Story::find($story_id);
        $tasks = $story->tasks()->get();
        $hours = 0;

        foreach($tasks as $task){
            $hours += $task->hours;
        }

        return view('project::backlog_task', [
            'project' => $project,
            'story' => $story,
            'hours' => $hours,
            'tasks' => $tasks
        ]);
    }
}
