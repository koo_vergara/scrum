<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'project', 'namespace' => 'Modules\Project\Http\Controllers'], function()
{
    Route::get('/', 'ProjectController@index');
    Route::post('/store', 'ProjectController@store');
    Route::get('/{id}', 'ProjectController@show');
    Route::get('/{id}/backlog', 'ProjectController@addBacklog');
    Route::get('/{project_id}/backlog/story/{story_id}', 'ProjectController@showBacklogStory');
    Route::get('/edit/{id}', 'ProjectController@edit');
    Route::post('/edit', 'ProjectController@update');
});
