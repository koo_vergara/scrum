@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col s12">
			<p><a href='{{ url("/project/$project->id") }}'>&laquo; Back to {{ $project->project_name }}</a></p>
		    <h1>{{ $project->project_name }}: Product Backlog</h1>

		    <div class="row">
		    	<div class="col s12">
			    	@foreach($stories as $story)
			    	<div class="col s12 m3">
			    		<div class="card small sticky-action blue-grey darken-1 hoverable">
			    			<div class="card-content white-text">
			    				<span class="card-title activator">
			    					{{ $story->complexity }} 
			    					<i class="material-icons right">more_vert</i></span>
			    				<p>{{ $story->user_story }}</p>

			    				@if($story->constraints)
			    				@php
			    					$constraints = json_decode($story->constraints);
			    				@endphp
			    				<ul class="browser-default">
			    					@foreach($constraints as $constraint)
			    					<li>{{ $constraint }}</li>
			    					@endforeach
			    				</ul>
			    				@endif
			    			</div>
			    			<div class="card-action">
			    				<a href='{{ url("/project/$project->id/backlog/story/$story->id") }}'><i class="material-icons">visibility</i></a>
			    			</div>
			    			<div class="card-reveal">
			    				<span class="card-title grey-text text-darken-4">Tasks <i class="material-icons right">close</i></span>
			    				@if($story->tasks()->count())
			    				<ol>
			    					@foreach($story->tasks()->get() as $tasks)
			    					<li>{{ $tasks->task }}</li>
			    					@endforeach
			    				</ol>
			    				@else
			    				<p>There are no tasks in this story.</p>
			    				@endif
			    			</div>
			    		</div>
			    	</div>
			    	@endforeach
			    </div>
		    </div>

		    <div class="fixed-action-btn horizontal">
		    	<button data-target="new-story-modal" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">add</i></button>
		    </div>

		    <div id="new-story-modal" class="modal">
				<div class="modal-content">
					<h4>New User Story</h4>
					<p>Project: {{ $project->project_name }}</p>
					<form method="post" action="{{ url('/story/store') }}" class="col s12" id="new-story">
		                <div class="row">
		                    <div class="input-field col s12">
		                        <textarea id="user_story" name="user_story" placeholder="As a <role>, I <action>, so that <achievement>." class="materialize-textarea"></textarea>
		                        <label for="user_story">User Story</label>
		                    </div>
		                    <div class="col s12">
		                    	<label>Constraints <a class="btn-floating btn-small add-constraints waves-effect waves-light red"><i class="material-icons md-18">add</i></a></label>
		                    	<input type="hidden" name="constraints" class="constraints" value="">
		                    	<input type="text" placeholder="Constraint" class="validate constraint constraint-0" data-ctr="0">
		                    </div>
		                    <div class="input-field col s8">
		                        <div class="row">
		                        	<div class="col s3">
		                        		<input class="with-gap" name="complexity" type="radio" id="small" value="2">
      									<label for="small">Small</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="medium" value="5">
      									<label for="medium">Medium</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="large" value="13">
      									<label for="large">Large</label>
      								</div>
      								<div class="col s3">
      									<input class="with-gap" name="complexity" type="radio" id="xlarge" value="34">
      									<label for="xlarge">XLarge</label>
      								</div>
      							</div>
		                    </div>
		                    <div class="input-field col s12">
		                    	<input type="hidden" name="project_id" value="{{ $project->id }}">
		                    	<button class="btn waves-effect waves-light" type="submit" name="action">Add</button>
		                	</div>
		                </div>
		            </form>
				</div>
			</div>

			<div id="burndown-chart-modal" class="modal">
				<div class="modal-content">
					<h4>Burndown Chart</h4>
					<canvas id="burndown"></canvas>
				</div>
			</div>
		</div>
	</div>
@stop
