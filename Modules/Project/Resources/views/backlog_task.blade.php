@extends('story::layouts.master')

@section('content')
	<div class="row">
		<div class="col s12">
			<p><a href='{{ url("/project/$project->id/backlog") }}'>&laquo; Back to {{ $project->project_name }} Product Backlog</a></p>
    			<!-- <a class="btn-floating btn-large waves-effect waves-light btn modal-trigger" href="#stories-modal"><i class="material-icons">apps</i></a> -->

		    <table>
		    	<thead>
		    		<tr>
		    			<th width="50%">User Story</th>
		    			<th width="25%">Complexity</th>
		    			<th width="25%">Hours</th>
		    		</tr>
		    	</thead>
		    	<tbody>
		    		<tr>
		    			<td>{{ $story->user_story }}</td>
		    			<td>{{ $story->complexity }}</td>
		    			<td>{{ $hours }}</td>
		    		</tr>
		    	</tbody>
		    </table>

		    <div class="row">
		    	<div class="col s3"><h4>Backlog</h4></div>
		    	<div class="col s3"><h4>Ongoing</h4></div>
		    	<div class="col s3"><h4>Test</h4></div>
		    	<div class="col s3"><h4>Done</h4></div>
		    </div>
		    @if($tasks->isEmpty())
		    <div class="row">
		    	<div class="col s12"><center>There are no items found.</center></div>
		    </div>
		    @else
		    <div class="row">
		    	<div class="col s3">
		    		@foreach($tasks as $task)
		    			<?php
	    				$log = '';
	    				$hours_spent = 0;
	    				
	    				foreach($task->logs($task->id)->get() as $logs){
	    					if($logs->log_type == 0){
	    						$log = $logs;
	    					}

							if($logs->log_type == 2){
								$hours_spent += $logs->current_val;
							}
	    				}
		    			?>
		    			@if($log->current_val == 0)
					<div class="row">
						<div class="col s12">
							<div class="card blue-grey darken-1">
								<div class="card-content white-text">
									<span class="card-title"><span class="hours-spent">{{ $hours_spent }}</span> / {{ $task->hours }}</span>
									<p>{{ $task->task }}</p>
								</div>
								<div class="card-action">
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-hours"><i class="material-icons">query_builder</i></a>
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-status"><i class="material-icons">label</i></a>
									<!-- <button data-target="" class="btn-floating waves-effect waves-light"><i class="material-icons">perm_identity</i></button> -->
								</div>
							</div>
						</div>
					</div>
						@endif
					@endforeach
		    	</div>
		    	<div class="col s3">
		    		@foreach($tasks as $task)
		    			<?php
	    				$log = '';
	    				$hours_spent = 0;
	    				
	    				foreach($task->logs($task->id)->get() as $logs){
	    					if($logs->log_type == 0){
	    						$log = $logs;
	    					}

							if($logs->log_type == 2){
								$hours_spent += $logs->current_val;
							}
	    				}
		    			?>
		    			@if($log->current_val == 1)
					<div class="row">
						<div class="col s12">
							<div class="card blue-grey darken-1">
								<div class="card-content white-text">
									<span class="card-title"><span class="hours-spent">{{ $hours_spent }}</span> / {{ $task->hours }}</span>
									<p>{{ $task->task }}</p>
								</div>
								<div class="card-action">
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-hours"><i class="material-icons">query_builder</i></a>
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-status"><i class="material-icons">label</i></a>
									<!-- <button data-target="" class="btn-floating waves-effect waves-light"><i class="material-icons">perm_identity</i></button> -->
								</div>
							</div>
						</div>
					</div>
						@endif
					@endforeach
		    	</div>
		    	<div class="col s3">
		    		@foreach($tasks as $task)
		    			<?php
	    				$log = '';
	    				$hours_spent = 0;
	    				
	    				foreach($task->logs($task->id)->get() as $logs){
	    					if($logs->log_type == 0){
	    						$log = $logs;
	    					}

							if($logs->log_type == 2){
								$hours_spent += $logs->current_val;
							}
	    				}
		    			?>
		    			@if($log->current_val == 2)
					<div class="row">
						<div class="col s12">
							<div class="card blue-grey darken-1">
								<div class="card-content white-text">
									<span class="card-title"><span class="hours-spent">{{ $hours_spent }}</span> / {{ $task->hours }}</span>
									<p>{{ $task->task }}</p>
								</div>
								<div class="card-action">
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-hours"><i class="material-icons">query_builder</i></a>
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-status"><i class="material-icons">label</i></a>
									<!-- <button data-target="" class="btn-floating waves-effect waves-light"><i class="material-icons">perm_identity</i></button> -->
								</div>
							</div>
						</div>
					</div>
						@endif
					@endforeach
		    	</div>
		    	<div class="col s3">
		    		@foreach($tasks as $task)
		    			<?php
	    				$log = '';
	    				$hours_spent = 0;
	    				
	    				foreach($task->logs($task->id)->get() as $logs){
	    					if($logs->log_type == 0){
	    						$log = $logs;
	    					}

							if($logs->log_type == 2){
								$hours_spent += $logs->current_val;
							}
	    				}
		    			?>
		    			@if($log->current_val == 3)
					<div class="row">
						<div class="col s12">
							<div class="card blue-grey darken-1">
								<div class="card-content white-text">
									<span class="card-title"><span class="hours-spent">{{ $hours_spent }}</span> / {{ $task->hours }}</span>
									<p>{{ $task->task }}</p>
								</div>
								<div class="card-action">
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-hours"><i class="material-icons">query_builder</i></a>
									<a href="javascript:void(0)" data-url='{{ url("/task/show/$task->id") }}' class="btn-floating waves-effect waves-light update-task-status"><i class="material-icons">label</i></a>
									<!-- <button data-target="" class="btn-floating waves-effect waves-light"><i class="material-icons">perm_identity</i></button> -->
								</div>
							</div>
						</div>
					</div>
						@endif
					@endforeach
		    	</div>
		    </div>
		    @endif

		    <div class="fixed-action-btn horizontal">
		    	<button data-target="new-task-modal" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">add</i></button>
		    </div>

		    <div id="new-task-modal" class="modal">
				<div class="modal-content">
					<h4>New Task</h4>
					<p>{{ $story->user_story }} </p>
					<form method="post" action="{{ url('/task/store') }}" class="col s12" id="new-task">
		                <div class="row">
		                    <div class="input-field col s12">
		                        <textarea id="task" name="task" class="materialize-textarea"></textarea>
		                        <label for="task">Task</label>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="col s2">
                    			<input class="with-gap" name="hours" type="radio" id="one" value="1">
								<label for="one">1</label>
							</div>
							<div class="col s2">
                    			<input class="with-gap" name="hours" type="radio" id="two" value="2">
								<label for="two">2</label>
							</div>
							<div class="col s2">
                    			<input class="with-gap" name="hours" type="radio" id="three" value="3">
								<label for="three">3</label>
							</div>
							<div class="col s2">
                    			<input class="with-gap" name="hours" type="radio" id="five" value="5">
								<label for="five">5</label>
							</div>
							<div class="col s2">
                    			<input class="with-gap" name="hours" type="radio" id="eight" value="8">
								<label for="eight">8</label>
							</div>
							<div class="col s2">
                    			<input class="with-gap" name="hours" type="radio" id="thirteen" value="13">
								<label for="thirteen">13</label>
							</div>
		                </div>
		                <div class="row">
		                    <div class="input-field col s12">
		                    	<input type="hidden" name="story_id" value="{{ $story->id }}">
		                    	<button class="btn waves-effect waves-light" type="submit" name="action">Add</button>
		                	</div>
		                </div>
		            </form>
				</div>
			</div>

			<div id="update-task-status-modal" class="modal">
				<div class="modal-content">
					<h4>Update Task Status</h4>
					<div class="row">
						<div class="col s12">
							<div class="row">
								<div class="col s6">
									<div class="row">
										<div class="col s2"><strong>Task</strong></div>
										<div class="col s10"><span class="task"></span></div>
									</div>
									<div class="row">
										<div class="col s2"><strong>Hours</strong></div>
										<div class="col s10"><span class="hours"></span></div>
									</div>
								</div>
								<div class="col s6">
									<form method="post" action="{{ url('/task/updateStatus') }}" class="col s12" id="update-task-status">
					                	<div class="row">
					                		<div class="col s6">
												<input class="with-gap current_status" name="current_status" type="radio" id="status-backlog" value="0" />
												<label for="status-backlog">Backlog</label>
											</div>
											<div class="col s6">
												<input class="with-gap current_status" name="current_status" type="radio" id="status-ongoing" value="1" />
												<label for="status-ongoing">Ongoing</label>
											</div>
										</div>
										<div class="row">
					                		<div class="col s6">
												<input class="with-gap current_status" name="current_status" type="radio" id="status-test" value="2" />
												<label for="status-test">Test</label>
											</div>
											<div class="col s6">
												<input class="with-gap current_status" name="current_status" type="radio" id="status-done" value="3" />
												<label for="status-done">Done</label>
											</div>
										</div>
					                    <div class="input-field col s12">
					                    	<input type="hidden" class="task_id" name="task_id" value="">
					                    	<input type="hidden" class="log_type" name="log_type" value="0">
					                    	<input type="hidden" class="previous_val" name="previous_val" value="">
					                    	<button class="btn waves-effect waves-light" type="submit" name="action">Update</button>
					                	</div>
									</form>
						        </div>
							</div>
							<!-- <hr>
							<h5>History</h5>
							<table>
								<thead>
									<tr>
										<th>Status</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table> -->
						</div>
					</div>
				</div>
			</div>

			<div id="update-task-hours-modal" class="modal">
				<div class="modal-content">
					<h4>Update Task Hours</h4>
					<div class="row">
						<div class="col s12">
							<div class="row">
								<div class="col s6">
									<div class="row">
										<div class="col s2"><strong>Task</strong></div>
										<div class="col s10"><span class="task"></span></div>
									</div>
									<div class="row">
										<div class="col s2"><strong>Hours</strong></div>
										<div class="col s10"><span class="hours"></span></div>
									</div>
								</div>
								<div class="col s6">
									<form method="post" action="{{ url('/task/updateHours') }}" class="col s12" id="update-task-hours">
										<div class="input-field col s3">
					                        <input id="hours_spent" name="hours_spent" type="number" step="0.5" class="validate" required="" aria-required="" value="0" min="0">
											<label for="hours_spent">Hours Spent</label>
										</div>
										<div class="input-field col s3">
					                        <input id="day_count" name="day_count" type="number" class="validate" required="" aria-required="" value="0" min="0">
											<label for="day_count">Day</label>
										</div>
					                    <div class="input-field col s12">
					                    	<input type="hidden" class="task_id" name="task_id" value="">
					                    	<input type="hidden" class="log_type" name="log_type" value="2">
					                    	<input type="hidden" class="previous_val" name="previous_val" value="">
					                    	<input type="hidden" class="remaining" name="remaining" value="">
					                    	<button class="btn waves-effect waves-light" type="submit" name="action">Update</button>
					                	</div>
									</form>
						        </div>
							</div>
							<!-- <hr>
							<h5>History</h5>
							<table>
								<thead>
									<tr>
										<th>Hours</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody></tbody>
							</table> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
