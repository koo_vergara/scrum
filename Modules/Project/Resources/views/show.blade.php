@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col s12">
				<p><a href="{{ url('/project') }}">&laquo; Back to Projects</a></p>
			    <h1 class="white-text">
			    	{{ $project->project_name }} 
			    	<button data-target="new-sprint-modal" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">directions_run</i></button>
			    	<a href='{{ url("/project/$project->id/backlog") }}' class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">view_module</i></a>
			    </h1>
			    
			    @if($project->description)
			    <p class="white-text"><strong>Description:</strong> {{ $project->description }}</p>
			    @endif

			    <div class="{{ $project->forecast['project']['color'] }} darken-1 white-text chip"><strong>{{ $project->forecast['project']['result'] }}</strong></div>
			    <div class="{{ $project->forecast['budget']['color'] }} darken-1 white-text chip"><strong>{{ $project->forecast['budget']['result'] }}</strong></div>

			    <table class="striped white">
			    	<thead>
			    		<tr>
			    			<th>&nbsp;</th>
			    			<th>No. of Devs</th>
			    			<th>Hours per Dev</th>
			    			<th>Complexity</th>
			    			<th>Velocity</th>
			    			<th>Actual/Estimated Hours</th>
			    			<th>Start Date</th>
			    			<th>&nbsp;</th>
			    		</tr>
			    	</thead>
			    	<tbody>
			    		@if($sprints->isEmpty())
			    		<tr>
			    			<!-- <td colspan="8">There are no items found.</td> -->
			    			<td colspan="8">There are no items found.</td>
			    		</tr>
			    		@else
			    			@foreach($sprints as $sprint)
			    				<?php
			    				$complexity = 0;
			    				$hours = 0;
			    				$velocity = 0;
			    				$actual = 0;

								foreach($sprint->stories()->get() as $story){
									$complexity += $story->complexity;
									$completed_tasks = 0;
									$tasks = $story->tasks()->get();

									foreach($tasks as $task){
										$hours += $task->hours;
										$task_log = $task->logs($task->id)->where('log_type', 0)->orderBy('id', 'desc')->first();
										$story_pts = 0;

										if($task_log->current_val >= 3){
											$completed_tasks += 1;
										}

										$hours_logs = $task->logs($task->id)->where('log_type', 2)->get();

										foreach($hours_logs as $hours_log){
											$actual += $hours_log->current_val;
										}
									}

									if($tasks->count() == $completed_tasks){
					                    $velocity += $story->complexity;
					                }
								}
			    				?>
			    		<tr>
			    			<td><a href="javascript:void(0)" data-url='{{ url("/sprint/edit/$sprint->id") }}' class="edit-sprint">Sprint {{ $sprint->sprint_number }}</a></td>
			    			<td>{{ $sprint->dev_count }}</td>
			    			<td>{{ $sprint->dev_hours }}</td>
			    			<td>{{ $complexity }}</td>
			    			<td>{{ $velocity }}</td>
			    			<td>{{ $actual }}/{{ $hours }}</td>
			    			<td>{{ $sprint->start }}</td>
			    			<td>
			    				<a href='{{ url("/sprint/$sprint->id") }}'><i class="material-icons">assignment</i></a>
			    				<a href='{{ url("/sprint/$sprint->id") }}'><i class="material-icons">chevron_right</i></a>
			    			</td>
			    		</tr>
			    			@endforeach
			    		@endif
			    	</tbody>
			    </table>

			    <div id="new-sprint-modal" class="modal">
					<div class="modal-content">
						<h4>New Sprint</h4>
						<p>Project: {{ $project->project_name }}</p>
						<form method="post" action="{{ url('/sprint/store') }}" class="col s12" id="new-sprint">
							{{ csrf_field() }}
			                <div class="row">
			                	<div class="input-field col s2">
	                        		<input id="sprint_number" name="sprint_number" type="number" min="{{ $sprints->count() }}" value="{{ $sprints->count() + 1 }}" class="validate" required="" aria-required="">
	                        		<label for="sprint_number">Sprint Number</label>
	                        	</div>
			                    <div class="input-field col s2">
	                        		<input id="dev_count" name="dev_count" type="number" class="validate" required="" aria-required="">
	                        		<label for="dev_count">No. of Devs</label>
	                        	</div>
			                    <div class="input-field col s2">
	                        		<input id="dev_hours" name="dev_hours" type="number" class="validate" required="" aria-required="">
	                        		<label for="dev_hours">Hours per Dev</label>
	                        	</div>
	                        	<div class="input-field col s4">
	                        		<input id="start_date" name="start_date" type="text" class="datepicker">
	                        		<label for="start_date">Start Date</label>
	                        	</div>
	                        </div>
	                        <div class="row">
			                    <div class="input-field col s12">
			                    	<input type="hidden" name="project_id" value="{{ $project->id }}">
			                    	<button class="btn waves-effect waves-light" type="submit" name="action">Add</button>
			                	</div>
			                </div>
			            </form>
					</div>
				</div>

				<div id="edit-sprint-modal" class="modal">
					<div class="modal-content">
						<h4>Edit Sprint</h4>
						<p>Project: {{ $project->project_name }}</p>
						<form method="post" action="{{ url('/sprint/edit') }}" class="col s12" id="edit-sprint">
							{{ csrf_field() }}
			                <div class="row">
			                	<div class="input-field col s2">
	                        		<input id="edit_sprint_number" name="sprint_number" type="number" class="validate" required="" aria-required="">
	                        		<label for="edit_sprint_number">Sprint Number</label>
	                        	</div>
			                    <div class="input-field col s2">
	                        		<input id="edit_dev_count" name="dev_count" type="number" class="validate" required="" aria-required="">
	                        		<label for="edit_dev_count">No. of Devs</label>
	                        	</div>
			                    <div class="input-field col s2">
	                        		<input id="edit_dev_hours" name="dev_hours" type="number" class="validate" required="" aria-required="">
	                        		<label for="edit_dev_hours">Hours per Dev</label>
	                        	</div>
	                        	<div class="input-field col s4">
	                        		<input id="edit_start_date" name="start_date" type="text" class="datepicker">
	                        		<label for="edit_start_date">Start Date</label>
	                        	</div>
	                        </div>
	                        <div class="row">
			                    <div class="input-field col s12">
			                    	<input type="hidden" name="sprint_id" id="edit_sprint_id">
			                    	<input type="hidden" name="project_id" id="edit_project_id">
			                    	<button class="btn waves-effect waves-light" type="submit" name="action">Save</button>
			                	</div>
			                </div>
			            </form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
