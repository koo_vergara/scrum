@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col s12">
		    <h1 class="white-text">Projects <button data-target="new-project-modal" class="btn-floating btn-large waves-effect waves-light"><i class="material-icons">add</i></button></h1>

		    <table class="striped white">
		    	<thead>
		    		<tr>
		    			<th>Project Name</th>
		    			<th>Description</th>
		    			<th>Repository URL</th>
		    			<th>Project Forecast</th>
		    			<th>Budget Forecast</th>
		    			<th>Created By</th>
		    			<th>&nbsp;</th>
		    		<tr>
		    	</thead>
		    	<tbody>
		    		@if($projects->isEmpty())
		    		<tr>
		    			<td colspan="7">There are no items found.</td>
		    		</tr>
		    		@else
		    			@foreach($projects as $project)
		    		<tr>
		    			<td>
		    				@if(Auth::id() == $project->created_by)
		    				<a href="javascript:void(0)" data-url='{{ url("/project/edit/$project->id") }}' class="edit-project">{{ $project->project_name }}</a>
		    				@else
		    				{{ $project->project_name }}
		    				@endif
		    			</td>
		    			<td>{{ $project->description }}</td>
		    			<td><a href="{{ $project->repository_url }}" target="_blank">{{ $project->repository_url }}</a></td>
		    			<td><div class="{{ $project->forecast['project']['color'] }} darken-1 white-text chip"><strong>{{ $project->forecast['project']['result'] }}</strong></div></td>
		    			<td><div class="{{ $project->forecast['budget']['color'] }} darken-1 white-text chip"><strong>{{ $project->forecast['budget']['result'] }}</strong></div></td>
		    			<td>{{ $project->creator }}</td>
		    			<td><a href='{{ url("/project/$project->id") }}'><i class="material-icons">chevron_right</i></a></td>
		    		</tr>
		    			@endforeach
		    		@endif
		    	</tbody>
		    </table>

		    <div id="new-project-modal" class="modal">
				<div class="modal-content">
					<h4>New Project</h4>
					<form method="post" action="{{ url('/project/store') }}" class="col s12" id="new-project">
						{{ csrf_field() }}
		                <div class="row">
		                    <div class="input-field col s12">
		                        <input id="project_name" name="project_name" type="text" class="validate" required="" aria-required="">
		                        <label for="project_name">Project Name</label>
		                    </div>
		                </div>
		               	<div class="row">
		                    <div class="input-field col s12">
		                        <textarea id="project_desc" name="project_desc" class="materialize-textarea"></textarea>
		                        <label for="project_desc">Description</label>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="input-field col s12">
		                        <input id="repository_url" name="repository_url" type="text" class="validate">
		                        <label for="repository_url">Repository URL</label>
		                    </div>
		                </div>
		                <div class="row">
		                    <div class="input-field col s12">
		                    	<input type="hidden" name="created_by" value="{{ Auth::id() }}">
		                    	<button class="btn waves-effect waves-light" type="submit" name="action">Add</button>
		                	</div>
		                </div>
		            </form>
				</div>
			</div>
			<div id="edit-project-modal" class="modal">
				<div class="modal-content">
					<h4>Edit Project</h4>
					<form method="post" action="{{ url('/project/edit') }}" class="col s12" id="edit-project">
						{{ csrf_field() }}
			            <div class="row">
			                <div class="input-field col s12">
			                    <input id="edit_project_name" name="project_name" type="text" class="validate" required="" aria-required="">
			                    <label for="edit_project_name">Project Name</label>
			                </div>
			            </div>
			           	<div class="row">
			                <div class="input-field col s12">
			                    <textarea id="edit_project_desc" name="project_desc" class="materialize-textarea"></textarea>
			                    <label for="edit_project_desc">Description</label>
			                </div>
			            </div>
			            <div class="row">
			                <div class="input-field col s12">
			                    <input id="edit_repository_url" name="repository_url" type="text" class="validate">
			                    <label for="edit_repository_url">Repository URL</label>
			                </div>
			            </div>
			            <div class="row">
			                <div class="input-field col s12">
			                	<input type="hidden" name="project_id" id="edit_project_id">
			                	<button class="btn waves-effect waves-light" type="submit" name="action">Save</button>
			            	</div>
			            </div>
			        </form>
				</div>
			</div>
		</div>
	</div>
@stop
