<?php

Route::group(['as' => 'login', 'middleware' => 'web', 'prefix' => 'login', 'namespace' => 'Modules\Login\Http\Controllers'], function()
{
    Route::get('/', 'LoginController@index');
    Route::get('/logout', 'LoginController@logout');
    Route::post('/login', 'LoginController@login');
});
