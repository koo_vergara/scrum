@extends('layouts.login')

@section('content')
    <div id="login-page" class="row valign-wrapper">
    	<div class="col s12">
    		<div class="login-logo">
    			<img src="{{ asset('images/logo.png') }}">
    		</div>
    		<div class="z-depth-4 card-panel row">
    			<h5 class="center-align">Scrum System</h5>
    			<form class="col s12 center-align" method="post" action="{{ asset('/login') }}">
    				<div class="row">
						<div class="input-field">
							<input id="email" name="email" type="email" class="validate">
							<label for="email">Email</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field">
							<input id="password" type="password" class="validate">
							<label for="password">Password</label>
						</div>
					</div>
					<p>
						<input type="checkbox" class="filled-in" id="remember_me" checked="checked" />
						<label for="remember_me">Keep me logged in</label>
					</p>
					<button class="btn waves-effect waves-light" type="submit" name="action">Login</button>
    			</form>
    			<div class="col s12">
    				<div class="google center-align">
    					<a href="{{ url('login/google') }}" class="waves-effect waves-light btn red btn-social btn-google"><i class="fa fa-google"></i> Sign in using Google</a>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
@stop
